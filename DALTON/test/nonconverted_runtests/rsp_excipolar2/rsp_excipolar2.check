#!/bin/ksh
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

if $GREP -q "not implemented for parallel calculations" $log; then
   echo "TEST ENDED AS EXPECTED"
   exit 0
fi

# Basis set
CRIT1=`$GREP "H * 1 * 1\.0000 * 4 * 2 * \[4s\|2s\]" $log | wc -l`
CRIT2=`$GREP "Li * 1 * 3\.0000 * 17 * 9 * \[8s3p\|3s2p\]" $log | wc -l`
CRIT3=`$GREP "total\: * 2 * 4\.0000 * 21 * 11" $log | wc -l`
TEST[1]=`expr	$CRIT1 \+ $CRIT2 \+ $CRIT3`
CTRL[1]=3
ERROR[1]="BASIS SET NOT READ CORRECTLY"

# Geometry
CRIT1=`$GREP "Total number of coordinates\: * 6" $log | wc -l`
CRIT2=`$GREP "1 * x * (0| )\.0000000000" $log | wc -l`
CRIT3=`$GREP "2 * y * (0| )\.0000000000" $log | wc -l`
CRIT4=`$GREP "3 * z * 2\.0969699107" $log | wc -l`
CRIT5=`$GREP "4 * x * (0| )\.0000000000" $log | wc -l`
CRIT6=`$GREP "5 * y * (0| )\.0000000000" $log | wc -l`
CRIT7=`$GREP "6 * z * (\-0|\-)\.9969699107" $log | wc -l`
TEST[2]=`expr	$CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6 \+ \
		$CRIT7`
CTRL[2]=7
ERROR[2]="GEOMETRY NOT READ CORRECTLY"

# Symmetry
CRIT1=`$GREP "Number of coordinates in each symmetry\: * 2 * 2 * 2 * 0" $log | wc -l`
CRIT2=`$GREP "Number of orbitals in each symmetry\: * 7 * 2 * 2 * 0" $log | wc -l`
TEST[3]=`expr	$CRIT1 \+ $CRIT2`
CTRL[3]=2
ERROR[3]="SYMMETRY NOT CORRECT"

# Energies
CRIT1=`$GREP "Hartree\-Fock total energy *\: * \-7\.9773493(6|7).." $log | wc -l`
CRIT2=`$GREP "\= MP2 second order energy *\: * \-7\.9901927.." $log | wc -l`
CRIT3=`$GREP "Final MCSCF energy\: * \-7\.9941305(2|3)...." $log | wc -l`
TEST[4]=`expr	$CRIT1 \+ $CRIT2 \+ $CRIT3`
CTRL[4]=3
ERROR[4]="ENERGIES NOT CORRECT"

# Response setup
CRIT1=`$GREP "Cubic response two\-photon absorption TPAMP\=T" $log | wc -l`
CRIT2=`$GREP "Excited state polarizability requested TPALP\=T" $log | wc -l`
CRIT3=`$GREP "1 B\-frequencies * 3\.000000(D|E)\-02" $log | wc -l`
CRIT4=`$GREP "1 A OPERATORS OF SYMMETRY NO\: * [1-3] AND LABELS\:" $log | wc -l`
CRIT5=`$GREP "1 B OPERATORS OF SYMMETRY NO\: * [1-3] AND LABELS\:" $log | wc -l`
TEST[5]=`expr	$CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5`
CTRL[5]=9
ERROR[5]="RESPONSE CALCULATION NOT SET UP CORRECTLY"

# Polarizability (ZDIPLEN - Sym 1, State 1 - Sym 1)
CRIT1=`$GREP "Na T\[4\] Nb Nc Nd * 263\.50.* 263\.50" $log | wc -l`
CRIT2=`$GREP "Na T\[3\] Nx Nyz * \-185\.58.* 77\.91" $log | wc -l`
CRIT3=`$GREP "Na X\[3\] Ny Nz * \-33\.53.* 44\.38" $log | wc -l`
CRIT4=`$GREP "Nx A\[3\] Ny Nz * \-25\.02.* 19\.36" $log | wc -l`
CRIT5=`$GREP "Na X\[2\] Nyz * \-19\.92.* (\-0|\-)\.56" $log | wc -l`
CRIT6=`$GREP "Nx A\[2\] Nyz * 15\.39.* 14\.82" $log | wc -l`
CRIT7=`$GREP "\@ State no\.\, symmetry\, excitation energy\: * 1 * 1 * 0*\.11958" $log | wc -l`
CRIT8=`$GREP "\@ < e \| AB \| f > * \= * 14\.82" $log | wc -l`
TEST[6]=`expr	$CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6 \+ \
		$CRIT7 \+ $CRIT8`
CTRL[6]=13
ERROR[6]="EXCITED STATE POLARIZABILITY (ZDIPLEN - SYM 1, STATE 1 - SYM 1) NOT CORRECT"

# Polarizability (ZDIPLEN - Sym 1, State 2 - Sym 1)
CRIT1=`$GREP "Na T\[4\] Nb Nc Nd * 137\.46.* * 137\.46....." $log | wc -l`
CRIT2=`$GREP "Na T\[3\] Nx Nyz * \-121\.44.* * 16\.02....." $log | wc -l`
CRIT3=`$GREP "Na X\[3\] Ny Nz * 27\.81.* * 43\.83....." $log | wc -l`
CRIT4=`$GREP "Nx A\[3\] Ny Nz * 34\.78.* * 78\.62....." $log | wc -l`
CRIT5=`$GREP "Na X\[2\] Nyz * \-18\.27.* * 60\.35....." $log | wc -l`
CRIT6=`$GREP "Nx A\[2\] Nyz * \-154\.54.* * \-94\.19....." $log | wc -l`
CRIT7=`$GREP "\@ State no\.\, symmetry\, excitation energy\: * 2 * 1 * (0| )\.27218[0-9]" $log | wc -l`
CRIT8=`$GREP "\@ < e \| AB \| f > * \= * \-94\.19" $log | wc -l`
TEST[7]=`expr	$CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6 \+ \
		$CRIT7 \+ $CRIT8`
CTRL[7]=13
ERROR[7]="EXCITED STATE POLARIZABILITY (ZDIPLEN - SYM 1, STATE 2 - SYM 1) NOT CORRECT"

# Polarizability (XDIPLEN - Sym 2, State 1 - Sym 1)
CRIT1=`$GREP "Na T\[4\] Nb Nc Nd * 100\.47.* 100\.47" $log | wc -l`
CRIT2=`$GREP "Na T\[3\] Nx Nyz * \-21\.70.* 78\.76" $log | wc -l`
CRIT3=`$GREP "Na X\[3\] Ny Nz * 13\.41.* 92\.18" $log | wc -l`
CRIT4=`$GREP "Nx A\[3\] Ny Nz * 26\.94.* 119\.13" $log | wc -l`
CRIT5=`$GREP "Na X\[2\] Nyz * \-44\.50.* 74\.62" $log | wc -l`
CRIT6=`$GREP "Nx A\[2\] Nyz * \-372\.9.* \-298\.32" $log | wc -l`
CRIT7=`$GREP "\@ State no\.\, symmetry\, excitation energy\: * 1 * 1 * (0| )\.11958[0-9]" $log | wc -l`
CRIT8=`$GREP "\@ < e \| AB \| f > * \= * \-298\.32" $log | wc -l`
TEST[8]=`expr	$CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6 \+ \
		$CRIT7 \+ $CRIT8`
CTRL[8]=20
ERROR[8]="EXCITED STATE POLARIZABILITY (XDIPLEN - SYM 2, STATE 1 - SYM 1) NOT CORRECT"

# Polarizability (XDIPLEN - Sym 2, State 2 - Sym 1)
CRIT1=`$GREP "Na T\[4\] Nb Nc Nd * 64\.46.* 64\.46" $log | wc -l`
CRIT2=`$GREP "Na T\[3\] Nx Nyz * \-24\.44.* 40\.02" $log | wc -l`
CRIT3=`$GREP "Na X\[3\] Ny Nz * 15\.78.* 55\.8[01]" $log | wc -l`
CRIT4=`$GREP "Nx A\[3\] Ny Nz * 27\.52.* 83\.33" $log | wc -l`
CRIT5=`$GREP "Na X\[2\] Nyz * \-36\.45.* 46\.8[78]" $log | wc -l`
CRIT6=`$GREP "Nx A\[2\] Nyz * \-51\.78.* \-4\.90" $log | wc -l`
CRIT7=`$GREP "\@ State no\.\, symmetry\, excitation energy\: * 2 * 1 * (0| )\.27218[0-9]" $log | wc -l`
CRIT8=`$GREP "\@ < e \| AB \| f > * \= * \-4\.90" $log | wc -l`
TEST[9]=`expr	$CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6 \+ \
		$CRIT7 \+ $CRIT8`
CTRL[9]=20
ERROR[9]="EXCITED STATE POLARIZABILITY (XDIPLEN - SYM 2, STATE 2 - SYM 1) NOT CORRECT"

# Polarizability (YDIPLEN - Sym 3, State 1 - Sym 1)
CRIT1=`$GREP "Na T\[4\] Nb Nc Nd * 100\.47.* 100\.47" $log | wc -l`
CRIT2=`$GREP "Na T\[3\] Nx Nyz * \-21\.70.* 78\.76" $log | wc -l`
CRIT3=`$GREP "Na X\[3\] Ny Nz * 13\.41.* 92\.18" $log | wc -l`
CRIT4=`$GREP "Nx A\[3\] Ny Nz * 26\.94.* 119\.13" $log | wc -l`
CRIT5=`$GREP "Na X\[2\] Nyz * \-44\.50.* 74\.62" $log | wc -l`
CRIT6=`$GREP "Nx A\[2\] Nyz * \-372\.9.* \-298\.32" $log | wc -l`
CRIT7=`$GREP "\@ State no\.\, symmetry\, excitation energy\: * 1 * 1 * 0*\.11958" $log | wc -l`
CRIT8=`$GREP "\@ < e \| AB \| f > * \= * \-298\.32" $log | wc -l`
TEST[10]=`expr	$CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6 \+ \
		$CRIT7 \+ $CRIT8`
CTRL[10]=20
ERROR[10]="EXCITED STATE POLARIZABILITY (YDIPLEN - SYM 3, STATE 1 - SYM 1) NOT CORRECT"

# Polarizability (YDIPLEN - Sym 3, State 2 - Sym 1)
CRIT1=`$GREP "Na T\[4\] Nb Nc Nd * 64\.46.* 64\.46" $log | wc -l`
CRIT2=`$GREP "Na T\[3\] Nx Nyz * \-24\.44.* 40\.02" $log | wc -l`
CRIT3=`$GREP "Na X\[3\] Ny Nz * 15\.78.* 55\.8[01]" $log | wc -l`
CRIT4=`$GREP "Nx A\[3\] Ny Nz * 27\.52.* 83\.33" $log | wc -l`
CRIT5=`$GREP "Na X\[2\] Nyz * \-36\.45.* 46\.8[78]" $log | wc -l`
CRIT6=`$GREP "Nx A\[2\] Nyz * \-51\.78.* \-4\.90" $log | wc -l`
CRIT7=`$GREP "\@ State no\.\, symmetry\, excitation energy\: * 2 * 1 * (0| )\.27218" $log | wc -l`
CRIT8=`$GREP "\@ < e \| AB \| f > * \= * \-4\.90" $log | wc -l`
TEST[11]=`expr	$CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6 \+ \
		$CRIT7 \+ $CRIT8`
CTRL[11]=20
ERROR[11]="EXCITED STATE POLARIZABILITY (YDIPLEN - SYM 3, STATE 2 - SYM 1) NOT CORRECT"

# Polarizability (ZDIPLEN - Sym 1, State 1 - Sym 2)
CRIT1=`$GREP "Na T\[4\] Nb Nc Nd * 107\.62.* 107\.62" $log | wc -l`
CRIT2=`$GREP "Na T\[3\] Nx Nyz * \-66\.96.* 40\.66" $log | wc -l`
CRIT3=`$GREP "Na X\[3\] Ny Nz * 1\.08.* 41\.75" $log | wc -l`
CRIT4=`$GREP "Nx A\[3\] Ny Nz * 10\.16.* 51\.91" $log | wc -l`
CRIT5=`$GREP "Na X\[2\] Nyz * \-29\.12.* 22\.79" $log | wc -l`
CRIT6=`$GREP "Nx A\[2\] Nyz * \-8\.90.* 13\.88" $log | wc -l`
CRIT7=`$GREP "\@ State no\.\, symmetry\, excitation energy\: * 1 * 2 * 0*\.15776" $log | wc -l`
CRIT8=`$GREP "\@ < e \| AB \| f > * \= * 13\.88" $log | wc -l`
TEST[12]=`expr	$CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6 \+ \
		$CRIT7 \+ $CRIT8`
CTRL[12]=13
ERROR[12]="EXCITED STATE POLARIZABILITY (ZDIPLEN - SYM 1, STATE 1 - SYM 2) NOT CORRECT"

# Polarizability (XDIPLEN - Sym 2, State 1 - Sym 2)
CRIT1=`$GREP "Na T\[4\] Nb Nc Nd * 239\.46.* 239\.46" $log | wc -l`
CRIT2=`$GREP "Na T\[3\] Nx Nyz * \-225\.13.* 14\.32" $log | wc -l`
CRIT3=`$GREP "Na X\[3\] Ny Nz * 90\.81.* 105\.14" $log | wc -l`
CRIT4=`$GREP "Nx A\[3\] Ny Nz * 112\.10.* 217\.24" $log | wc -l`
CRIT5=`$GREP "Na X\[2\] Nyz * \-57\.29.* 159\.95" $log | wc -l`
CRIT6=`$GREP "Nx A\[2\] Nyz * 174\.68.* 334\.64" $log | wc -l`
CRIT7=`$GREP "\@ State no\.\, symmetry\, excitation energy\: * 1 * 2 * 0*\.15776" $log | wc -l`
CRIT8=`$GREP "\@ < e \| AB \| f > * \= * 334\.64" $log | wc -l`
TEST[13]=`expr	$CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6 \+ \
		$CRIT7 \+ $CRIT8`
CTRL[13]=13
ERROR[13]="EXCITED STATE POLARIZABILITY (XDIPLEN - SYM 2, STATE 1 - SYM 2) NOT CORRECT"

# Polarizability (YDIPLEN - Sym 3, State 1 - Sym 2)
CRIT1=`$GREP "Na T\[4\] Nb Nc Nd * 117\.54.* 117\.54" $log | wc -l`
CRIT2=`$GREP "Na T\[3\] Nx Nyz * \-65\.06.* 52\.48" $log | wc -l`
CRIT3=`$GREP "Na X\[3\] Ny Nz * 36\.70.* 89\.18" $log | wc -l`
CRIT4=`$GREP "Nx A\[3\] Ny Nz * 53\.71.* 142\.90" $log | wc -l`
CRIT5=`$GREP "Na X\[2\] Nyz * \-57\.29.* 85\.60" $log | wc -l`
CRIT6=`$GREP "Nx A\[2\] Nyz * \-57\.26.* 28\.34" $log | wc -l`
CRIT7=`$GREP "\@ State no\.\, symmetry\, excitation energy\: * 1 * 2 * 0*\.15776" $log | wc -l`
CRIT8=`$GREP "\@ < e \| AB \| f > * \= * 28\.34" $log | wc -l`
TEST[14]=`expr	$CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6 \+ \
		$CRIT7 \+ $CRIT8`
CTRL[14]=13
ERROR[14]="EXCITED STATE POLARIZABILITY (YDIPLEN - SYM 3, STATE 1 - SYM 2) NOT CORRECT"

PASSED=1
for i in 1 2 3 4 5 6 7 8 9 10 11 12 13 14
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo "${ERROR[i]} ( test = ${TEST[i]}; control = ${CTRL[i]} ); "
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

