[![pipeline status](https://gitlab.com/dalton/dalton/badges/master/pipeline.svg)](https://gitlab.com/dalton/dalton/commits/master)
[![coverage report](https://gitlab.com/dalton/dalton/badges/master/coverage.svg)](https://gitlab.com/dalton/dalton/commits/master)

# Quick start

Clone the repository:
```
$ git clone --recursive git@gitlab.com:dalton/dalton-private.git
```

Build the code:
```
$ cd dalton
$ ./setup [--help]
$ cd build
$ make [-j4]
```

Run the test set:
```
$ ctest [-j4]
```

# How to contribute

See Dalton Developer’s Guide: http://dalton-devguide.readthedocs.io
